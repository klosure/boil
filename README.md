![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

### Motivation
I write a lot of code. When starting a new project there is always little bits and pieces I have to go around and grab when I want to start a new project. The goal of this project is to do all that **boilerplate** work for me with one command. I wanted to gear this towards C/C++ projects, and Shell projects. This tool is opinionated towards my preferred tools and workflow, but it can be easily swapped around to suit your preferences. 

</br>

> Your new project would look like this: 

```bash
user@comp $ boil -n MyNewProj -u C++ -l mit
user@comp $ tree -a MyNewProj
MyNewProj/
├── clang_complete
├── .git
├── .gitignore
├── include
├── LICENSE.md
├── makefile
├── MyNewProj.1
├── README.md
└── tests

3 directories, 6 files
```

### Dependencies

- [Git](https://git-scm.com/downloads)
- [ed](https://www.gnu.org/software/ed/manual/ed_manual.html) (the standard text editor)
- POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)
- POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)

### Installation
Simply run:

```bash
make deps
sudo make install

# to uninstall
sudo make uninstall
```
> make deps installs the *.boil* directory at $HOME

### Usage
```bash
boil                              # run in interactive mode
boil -n MyProj -u C -l bsd3       # setup C project named 'MyProj', with a BSD 3-Clause license
boil -n Deadbeef -u c++ -l art    # setup C++ project named 'Deadbeef', with a Artistic license
boil -h                           # show a help message
boil -v                           # show the version info

# valid licenses to use with -l
agpl                              # Affero General Public License
apache                            # Apache 2.0
art                               # Artistic (aka Perl) License
bsd0                              # BSD 0-Clause (ISC-esque)
bsd2                              # BSD 2-Clause
bsd3                              # BSD 3-Clause
bsd4                              # BSD 4-Clause (Original BSD License)
bsl                               # Boost Software License
epl                               # Eclipse Public License
gpl2                              # General Public License v2
gpl3                              # General Public License v3
isc                               # Internet Systems Consortium License
lgpl2                             # Lesser GPL v2
lgpl3                             # Lesser GPL v3
mit                               # MIT License
mpl                               # Mozilla Public License
un                                # The Unlicense
zlib                              # zlib/libpng License
```

### Notes
When we make the LICENSE.md file from template, we use the global git username (`git config --global user.name`) I would also like to expand this tool to allow for Python and Ruby projects.


### License / Disclaimer
This project is licensed under the 3-Clause BSD license. (See LICENSE.md)
Please make sure to cook thoroughly before consuming.
Artwork courtesy of macrovector. 
