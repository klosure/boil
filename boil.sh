#!/bin/sh
#
# author: klosure
# https://gitlab.com/klosure/boil
# license: bsd 3-clause
#
# Take care of setting up all the boilerplate for a new project
#
# Dependencies: POSIX shell, POSIX make, git, ed

# no arguments: interactive mode
# arguments: -n project_name, -u language, -l license


VERSION=0.2.6

# Badges
AGPL="[![License:](https:\/\/is.gd\/IlO3yO)](https:\/\/www.gnu.org\/licenses\/agpl-3.0)"
APCH="[![License](https:\/\/is.gd\/lkjFQD)](https:\/\/opensource.org\/licenses\/Apache-2.0)"
ART="[![License](https:\/\/is.gd\/VO5Z1Q)](https:\/\/opensource.org\/licenses\/Artistic-2.0)"
BSD0="[![License](https:\/\/is.gd\/p7NDCA)](https:\/\/opensource.org\/licenses\/0BSD)"
BSD2="[![License](https:\/\/is.gd\/ylDiC2)](https:\/\/opensource.org\/licenses\/BSD-2-Clause)"
BSD3="[![License](https:\/\/is.gd\/1M8LSi)](https:\/\/opensource.org\/licenses\/BSD-3-Clause)"
BSD4="[![License](https:\/\/is.gd\/WbYnjF)](https:\/\/www.openhub.net\/licenses\/bsd)"
BSL="[![License](https:\/\/is.gd\/1dSl1V)](https:\/\/www.boost.org\/users\/license.html)"
EPL="[![License](https:\/\/is.gd\/VF71lp)](https:\/\/opensource.org\/licenses\/EPL-1.0)"
GPL2="[![License](https:\/\/is.gd\/Z3mBg1)](https:\/\/www.gnu.org\/licenses\/old-licenses\/gpl-2.0.en.html)"
GPL3="[![License](https:\/\/img.shields.io\/badge\/License-GPLv3-blue.svg)](https:\/\/www.gnu.org\/licenses\/gpl-3.0)"
ISC="[![License](https:\/\/is.gd\/GbWFMI)](https:\/\/opensource.org\/licenses\/ISC)"
LGPL2="[![License](https:\/\/is.gd\/T3jab1)](https:\/\/www.gnu.org\/licenses\/old-licenses\/lgpl-2.1.en.html)"
LGPL3="[![License:LGPLv3](https:\/\/is.gd\/eBwIHR)](https:\/\/www.gnu.org\/licenses\/lgpl-3.0)"
MIT="[![License](https:\/\/img.shields.io\/badge\/License-MIT-yellow.svg)](https:\/\/opensource.org\/licenses\/MIT)"
MPL="[![License](https:\/\/is.gd\/rYpO9d)](https:\/\/opensource.org\/licenses\/MPL-2.0)"
UNLIC="[![License](https:\/\/img.shields.io\/badge\/license-Unlicense-blue.svg)](http:\/\/unlicense.org)"
ZLIB="[![License](https:\/\/is.gd\/JwIqeB)](https:\/\/opensource.org\/licenses\/Zlib)"

PROJ="NewProject"
CHOSEN_LANG="shell"
CLNG="clang_complete_cpp"


# edit readme with ed
# passed in badge and pretty license name
fun_edit_readme () {
    L_BADGE=$1
    L_PRETTY_LIC="$2"
    if [ -s ~/.boil/README.md ] ; then
	cp "$HOME"/.boil/README.md "$HOME"/.boil/README-tmp.md
	# insert project name
	printf ",s/MyProjectName/%s\nw\nq\n" "$PROJ" | ed "$HOME"/.boil/README-tmp.md > /dev/null 2>&1
	# insert license
	printf ",s/BadgeHere/%s\n,s/X/%s\nw\nq\n" "$L_BADGE" "$L_PRETTY_LIC" | ed "$HOME"/.boil/README-tmp.md > /dev/null 2>&1
    else
	fun_error "-- Error editing README.md --"
    fi
}

# edit makefile to reflect project name
fun_edit_make () {
    if [ -s ~/.boil/makefile-tmp ] ; then
	printf ",s/boil/%s/g\nw\nq\n" "$PROJ" | ed "$HOME"/.boil/makefile-tmp > /dev/null 2>&1
    else
	fun_error "-- Error editing makefile --"
    fi
}

# edit manpage to reflect project name
fun_edit_man () {
    if [ -s ~/.boil/man-tmp.1 ] ; then
	printf ",s/boil/%s\nw\nq\n" "$PROJ" | ed "$HOME"/.boil/man-tmp.1 > /dev/null 2>&1
    else
	fun_error "-- Error editing man page --"
    fi
}

fun_check_static_licen () {
    L_TOCHECK="$1"
    L_FOUND=1
    for F in ~/.boil/licenses/static/*.md ; do
	BASE_F=$(basename "$F")
	L_WOEXT=$(echo "$BASE_F" | cut -d. -f 1)
	if [ "$L_TOCHECK" = "$L_WOEXT" ] ; then
	    L_FOUND=0
	fi
    done
    unset L_TOCHECK L_WOEXT
    return $L_FOUND
}

# edit the license file to reflect your (git user) name
fun_edit_licen () {
    L_YEAR=$(date +"%Y")
    L_GIT_NAME="$(git config --global user.name)"
    if [ -s ~/.boil/LICENSE-tmp.md ] ; then
	printf ",s/<YEAR>/%s/g\n,s/<OWNER>/%s/g\nw\nq\n" "$L_YEAR" "$L_GIT_NAME" | ed "$HOME"/.boil/LICENSE-tmp.md > /dev/null 2>&1
    fi
    unset L_YEAR L_GIT_NAME
}

# set up one of the less common licenses
fun_setup_licen () {
    L_FSL="$1"
    if fun_check_static_licen "$L_FSL" ; then
	# found a static license; just copy it over
	cp ~/.boil/licenses/static/"$L_FSL.md" ~/.boil/LICENSE-tmp.md
    else
	# we need to edit a license template after copying
	cp ~/.boil/licenses/"$L_FSL.md" ~/.boil/LICENSE-tmp.md
	fun_edit_licen
    fi
    unset L_FSL
}


# parse user input of license
fun_set_licen_var () {
    L_LI_LOWER=$(echo "$1" | tr '[:upper:]' '[:lower:]')
    case "$L_LI_LOWER" in
	agpl) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$AGPL" "AGPL 3.0";;
	apache) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$APCH" "Apache 2.0";;
	art) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$ART" "Artistic 2.0";;
	bsd0) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$BSD0" "BSD 0-Clause";;
	bsd2) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$BSD2" "BSD 2-Clause";;
	bsd3) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$BSD3" "BSD 3-Clause";;
	bsd4) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$BSD4" "BSD 4-Clause";;
	bsl) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$BSL" "Boost Software";;
	epl) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$EPL" "EPL 2.0";;
	gpl2) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$GPL2" "GPLv2";;
	gpl3) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$GPL3" "GPLv3";;
	isc) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$ISC" "ISC";;
	lgpl2) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$LGPL2" "LGPLv2.1";;
	lgpl3) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$LGPL3" "LGPLv3";;
	mit) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$MIT" 'MIT';;
	mpl) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$MPL" "MPL 2.0";;
	un) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$UNLIC" "Unlicense";;
	zlib) fun_setup_licen "$L_LI_LOWER" && fun_edit_readme "$ZLIB" "zlib";;
	*) fun_error "-- Error parsing license --";;
    esac
    unset L_LI_LOWER
}


# what language is the project in?
fun_set_language () {
    L_ULOWER=$(echo "$1" | tr '[:upper:]' '[:lower:]')
    if [ "$L_ULOWER" = "c" ] ; then
	CHOSEN_LANG="c"
    elif [ "$L_ULOWER" = "c++" ] ; then
	CHOSEN_LANG="c++"
    elif [ "$L_ULOWER" = "cpp" ] ; then
	CHOSEN_LANG="c++"
    elif [ "$L_ULOWER" = "shell" ] ; then
	unset L_ULOWER
	return 0
    else
	unset L_ULOWER
	fun_error "-- Error setting language --"
    fi
    unset L_ULOWER
}


fun_set_clang () {
    # C or C++ ?
    if [ "$CHOSEN_LANG" = "c" ] ; then
	CLNG="clang_complete_c"
    elif [ "$CHOSEN_LANG" = "c++" ] ; then
	CLNG="clang_complete_c++"
    else
	return 1
    fi
    return 0
}


# set the name of out new project
fun_set_name () {
    PROJ="$1"
}


# prints out the version of boil
fun_print_ver () {
    echo "boil version: $VERSION"
}

# print general info
fun_print_info () {
    echo "boil - automate your boring new project setup process"
    fun_print_ver
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/boil"
    echo "license: bsd 3-clause"
}

# prints out the help menu
fun_print_help () {
    fun_print_info
    echo "--- usage --------------------------------------------------"
    echo "boil                             # start boil in interactive mode"
    echo "boil -n coolcode -u C -l mit     # setup C project named 'coolcode', with an MIT license"
    echo "boil -h                          # shows this help message"
    echo "boil -v                          # show the version info"
    echo ""
    echo "#(valid licenses: agpl|apache|art|bsd0|bsd2|bsd3|bsd4|bsl|epl|gpl2|gpl3|isc|lgpl2|lgpl3|mit|mpl|un|zlib)"
}


# prints out a general error message
fun_error () {
    echo "Uh Oh, something borked!"
    echo "$1"
    fun_print_help
    exit 1
}


fun_interactive () {
    L_PROJ=""
    L_GIVEN_LANG=""
    L_GIVEN_LIC=""
    # yes, we really do want only read, and not read -r. We DONT want backslashes.
    printf "> What do you wish to name your project?: "
    # shellcheck disable=SC2162
    read L_PROJ
    printf "> What language will this project be in? [C/C++/Shell]: "
    # shellcheck disable=SC2162
    read L_GIVEN_LANG
    printf "> What license will this project use?: "
    # shellcheck disable=SC2162
    read L_GIVEN_LIC
    fun_set_name "$L_PROJ"
    fun_set_language "$L_GIVEN_LANG"
    fun_set_licen_var "$L_GIVEN_LIC"
    echo "Creating files for $L_PROJ..."
    unset L_PROJ L_GIVEN_LANG L_GIVEN_LIC
}

# make sure that our core dependencies are installed
fun_check_deps () {
    if ! [ -x "$(command -v ed)" ]; then
	fun_error "error: boil needs ed, please install it."
    elif ! [ -x "$(command -v git)" ]; then
	fun_error "error: boil needs git, please install it."
    fi
}

# --- ENTRY POINT ---

fun_check_deps

# Did the user give any flags to us?
if [ "$#" = "0" ] ; then
    fun_print_info
    echo "Starting interactive mode..."
    fun_interactive
else
    # parse the user input
    while getopts 'n:u:l:vh' OPT ; do
	case "$OPT" in
	n)	fun_set_name "$OPTARG";;
	u)	fun_set_language "$OPTARG";;
	l)      fun_set_licen_var "$OPTARG";;
	v)      fun_print_ver && exit 0;;
	h)      fun_print_help && exit 0;;
	[?])	fun_error;;
	esac
    done
fi

# create the project dir
mkdir ./"$PROJ"

# clang_complete file (autocomplete for emacs)
if fun_set_clang ; then
    cp "$HOME"/.boil/clang/"$CLNG" "$PROJ"/clang_complete
fi

# move over the license file
mv ~/.boil/LICENSE-tmp.md "$PROJ"/LICENSE.md

# move over edited readme template
mv "$HOME"/.boil/README-tmp.md "$PROJ"/README.md

# copy temp makefile depending on lang and edit
if [ "$CHOSEN_LANG" = "c" ] ; then
    cp ~/.boil/make/makefile-c ~/.boil/makefile-tmp
elif [ "$CHOSEN_LANG" = "c++" ] ; then
    cp ~/.boil/make/makefile-cpp ~/.boil/makefile-tmp
else
    cp ~/.boil/make/makefile-shell ~/.boil/makefile-tmp
fi
    
# edit the makefile template
fun_edit_make
# move over the edited makefile
mv "$HOME"/.boil/makefile-tmp "$PROJ"/makefile

# copy manpage template
cp "$HOME"/.boil/man.1 "$HOME"/.boil/man-tmp.1
# edit the manpage
fun_edit_man
# move edited manpage
mv "$HOME"/.boil/man-tmp.1 "$PROJ/$PROJ.1"

# copy over a standard .gitignore file
cp "$HOME"/.boil/gitignore "$PROJ"/.gitignore

# initizalize a git repo
cd "$PROJ" || fun_error "error: could not cd into directory $PROJ"
git init > /dev/null 2>&1

# make 'include' directory for c/c++ projects
if [ "$CHOSEN_LANG" = "c" ] || [ "$CHOSEN_LANG" = "c++" ] ; then
    mkdir include
fi

mkdir tests

unset L_FOUND CHOSEN_LANG PROJ CLNG VERSION

exit 0
